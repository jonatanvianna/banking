FROM python:3.7-alpine

LABEL maintainer=jonatanvianna

ARG ENVIRONMENT=dev

ENV ENVIRONMENT=$ENVIRONMENT

WORKDIR /src

COPY requirements requirements/

# Defaults to dev
RUN if [ "$ENVIRONMENT" == "prod" ]; \
        then apk add --update --virtual .build-deps; \
        else apk add --update --virtual .build-deps gcc musl-dev; \
    fi \
    && pip install -U pip \
    && pip install --no-cache-dir -r requirements/$ENVIRONMENT.txt \
    && rm -rf requirements \
    && apk del .build-deps

COPY src/ /src/

# Runs All unit tests
CMD pytest --tb=no -r=A
