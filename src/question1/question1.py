from question1.exceptions import EmptyOpenContratList, InvalidTopN

from typing import List


class Contract:
    """Customer Contract"""

    def __init__(self, id, debt):
        self.id = id
        self.debt = debt

    def __str__(self):
        return f"id={self.id}, debt={self.debt}"

    def __repr__(self):
        return f"<Contract (id={self.id}, debt={self.debt})>"


class Contracts:
    """Contracts tasks"""

    def get_top_N_open_contracts(
        self, open_contracts: List[Contract], renegotiated_contracts: List[int], top_n: int
    ) -> List[int]:
        """
        :open_contracts: a list containing Contract instances
        :renegotiated_contracts: a list containing Contract ids
        :top_n: Top contracts that must be returned
        :raises: EmptyOpenContratList, InvalidTopN
        """
        if not open_contracts:
            raise EmptyOpenContratList

        if isinstance(top_n, int):
            if top_n <= 0:
                raise InvalidTopN
        else:
            raise InvalidTopN

        [
            open_contracts.remove(contract)
            for contract in [contract for contract in open_contracts if contract.id in renegotiated_contracts]
        ]

        return [
            contract.id
            for contract in sorted(open_contracts, key=lambda contract: contract.debt, reverse=True)[:top_n]
        ]
