import unittest

from parameterized import parameterized

from question1.question1 import Contract, Contracts
from question1.exceptions import EmptyOpenContratList, InvalidTopN


class TestGetTopNOpenContracts(unittest.TestCase):
    """Tests the method `get_top_N_open_contracts` from Contracts Class
       that returns a sorted list of Top N Open Contracts."""

    def setUp(self):
        self.open_contracts = [Contract(1, 1), Contract(2, 2), Contract(3, 3), Contract(4, 4), Contract(5, 5)]
        self.contracts = Contracts()
        self.top_n = 3

    def test_get_open_contracts(self):
        """Asserts the returned `Contract List` contains only open contracts."""
        renegotiated_contracts = [3]
        expected_contracts = [self.open_contracts[4].id, self.open_contracts[3].id, self.open_contracts[1].id]
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, self.top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    def test_empty_renegotiated_contracts_list(self):
        """Asserts an empty `Renegotiated Contracts` list returns top N `Open Contracts`."""

        renegotiated_contracts = []
        expected_contracts = [self.open_contracts[4].id, self.open_contracts[3].id, self.open_contracts[2].id]
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, self.top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    def test_all_open_contrats_renegotiated(self):
        """Asserts an empty `Open Contracts` list is returned when all contracts are renegotiated."""

        renegotiated_contracts = [1, 2, 3, 4, 5]
        expected_contracts = []
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, self.top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    def test_unordered_renegotiated_contracts_list(self):
        """Asserts an unordered `Renegotiated Contracts` list returns the `Open Contracts`."""

        renegotiated_contracts = [4, 1, 5]
        expected_contracts = [self.open_contracts[2].id, self.open_contracts[1].id]
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, self.top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    def test_unexistent_renegotiated_contracts_list(self):
        """Asserts an unexistent `Renegotiated Contracts` list returns top N `Open Contracts`."""

        renegotiated_contracts = [11, 8, 6, 45674, -273]
        expected_contracts = [self.open_contracts[4].id, self.open_contracts[3].id, self.open_contracts[2].id]
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, self.top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    def test_top_n_greater_then_contracts_list_length(self):
        """Asserts that a top N grater than `Open Contracts` length returns the available `Open Contracts`."""

        renegotiated_contracts = [3, 5]
        expected_contracts = [self.open_contracts[3].id, self.open_contracts[1].id, self.open_contracts[0].id]
        top_n = 42
        returned_contracts = self.contracts.get_top_N_open_contracts(
            self.open_contracts, renegotiated_contracts, top_n
        )
        self.assertEqual(returned_contracts, expected_contracts)

    @parameterized.expand([("top_n",), (-1,), (12.45,)])
    def test_none_top_n(self, top_n):
        """Asserts that a top N less 1 (one) or not an int raises an InvalidTopN Exception."""

        renegotiated_contracts = [4, 1]
        with self.assertRaises(InvalidTopN):
            self.contracts.get_top_N_open_contracts(self.open_contracts, renegotiated_contracts, top_n)
            self.assertEqual(InvalidTopN.message, "Contract List must not be empty")

    def test_empty_contracts_list(self):
        """Asserts an empty `Open Contracts` list raises an EmptyOpenContratList Exception."""

        open_contracts = []
        renegotiated_contracts = [2, 3]
        with self.assertRaises(EmptyOpenContratList):
            self.contracts.get_top_N_open_contracts(open_contracts, renegotiated_contracts, 3)
            self.assertEqual(EmptyOpenContratList.message, "Contract List must not be empty")


if __name__ == "__main__":
    unittest.main()
