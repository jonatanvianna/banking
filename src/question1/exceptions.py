class EmptyOpenContratList(Exception):
    message = "Contract List must not be empty."


class InvalidTopN(Exception):
    message = "Top N Open Contrats must be greater than zero."
