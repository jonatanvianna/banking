<H2>Technical challenge number 1<H2>

Suponha que um banco possui entre suas APIs de uso interno, as seguintes:
* Um serviço ("Operações em aberto") que gera a cada final de mês, uma lista dos associados e do saldo de suas operações 
financeiras que estão em atraso
* Outro serviço ("Renegociação") que retorna uma lista com os associados que já renegociaram seus atrasos

Suponha que, como parte de uma tarefa, você recebeu o trecho de código em anexo.
A classe <i>Contract</i> possui 2 atributos:
* <i>id</i>,  o identficador de um correntista e
* <i>debt</i>, o saldo devedor total (de todas as operações) de um correntista

Sua tarefa é implementar um método que retorne os <i>N</i> maiores devedores que ainda não possuem seus débitos renegociados.
 
O método <i>get_top_N_open_contracts</i> possui a assinatura proposta para esta tarefa. Ele recebe 3 parâmetros,
na seguinte ordem:
1. <i>open_contracts</i>: Uma lista em que cada elemento é uma instância de <i>Contract</i>,
2. <i>renegotiated_contracts</i>: Uma lista de numeros inteiros (<i>int</i>) representando os identificadores dos associados que já renegociaram seus débitos
3. <i>top_n</i>: Um inteiro com a quantidade de devedores que devem ser retornados pelo método.

Por fim, o método deve retornar uma lista contendo <i>top_n</i> inteiros referentes aos identificadores dos devedores,
ordenada do maior devedor para o menor, isto é, a posição 0 terá o maior devedor e a posição <i>top_n</i> -1 o menor.

Exemplos de uso:

    contracts = [
            Contract(1, 1),
            Contract(2, 2),
            Contract(3, 3),
            Contract(4, 4),
            Contract(5, 5)
        ]

        renegotiated = [3]
        top_n = 3
        
        actual_open_contracts = get_top_N_open_contracts(contracts, renegotiated, top_n)
        
        expected_open_contracts = [5, 4, 2]
        
        assert expected_open_contracts == actual_open_contracts