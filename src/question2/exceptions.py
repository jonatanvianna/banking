class InvalidRequest(Exception):
    message = "Invalid values in requests list. Must be a List of positive integers."


class InvalidNMax(Exception):
    message = "Invalid values for maximum stops quantity. Must be a positive integer."
