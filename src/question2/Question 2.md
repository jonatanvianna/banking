<H2>Technical challenge number 2<H2>

Suponha que um banco possua um serviço que permita a suas agências fazer requisições de valores monetários (dinheiro)
que serão atendidos por uma central de distribuição através do envio de carros-forte para efetuar as entregas.

Considerado que este banco busca otimizar seus recursos e deseja diminuir a quantidade de viagens de carros-forte,
decidiu-se por unir pedidos de agências próximas a serem entregues na mesma viagem. No entanto, por questões de segurança,
são permitidas a união de no máximo 2 pedidos por viagem e desde que não excedam um valor monetário máximo por viagem.

Sua tarefa é implementar um método que faça o cálculo do número mínimo de viagens, dadas umas lista de requisições
(assuma que cada chamada do método contém apenas requisições de agências próximas) e um valor máximo.

O método <i>combine_orders</i>, da classe <i>Orders</i>, em anexo possui a assinatura proposta.
* <i>requests</i> é uma lista de inteiros, representando o valor monetário requisitado por uma agência.
* <i>n_max</i> é um inteiro contendo o valor máximo que pode ser levado em uma única viagem.
* Lembre que cada viagem atende no máximo a 2 requisições.
* Assuma ainda que nenhum valor dentro de <i>requests</i> será superior a <i>n_max</i>.

Espera-se como retorno apenas um valor inteiro, contendo o número mínimo de viagens que deve ser
feita para atender todas as requisições.
Não é preciso informar quais requisições serão combinadas, apenas a quantidade mínima de viagens.

Ex:

    orders = [70, 30, 10]
    n_max = 100
    expected_orders = 2
    how_many = Orders().combine_orders(orders, n_max)

    assert how_many == expected_orders