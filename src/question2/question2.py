from typing import List

from question2 import validators


class Orders:
    max_stops = 2

    def combine_orders(self, requests: List[int], n_max: int) -> int:
        """Calculates the minimum number of stops from a delivery money request."""
        validators.validate_n_max(n_max)
        validators.validate_requests(requests)

        def calculate_larger_request(request):
            """Calculates the number of stops if a request is larger than max stops."""

            quotient = request / n_max
            base = int(quotient) * n_max

            return int(quotient), request - base

        def calculate_minor_requests(requests):
            """Calculates the number of stops for requests minor than max stops."""

            # removes possible zeros remaining from larger requests calculation
            [requests.pop(request) for request in requests if request == 0]

            count_max_stops = 0

            while requests:
                first = requests.pop(0)
                count_max_stops += 1
                indexes_for_popping = []

                for index, request in enumerate(requests):
                    if first + request <= n_max:
                        first += request
                        indexes_for_popping.append(index)

                [requests.pop() for i in indexes_for_popping]

            return count_max_stops

        count_total_stops = 0
        for index, request in enumerate(requests):
            if request > n_max:
                quotient, base = calculate_larger_request(request)
                count_total_stops += quotient
                requests[index] = base

        return count_total_stops + calculate_minor_requests(requests)
