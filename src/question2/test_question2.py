import unittest

from parameterized import parameterized

from question2.exceptions import InvalidRequest, InvalidNMax
from question2.question2 import Orders


class TestCombineOrders(unittest.TestCase):
    """Tests the method `combine_orders` from Orders Class
       that returns the minimum number of delivery stops."""

    def test_challenge_example(self):
        requests = [70, 30, 10]
        n_max = 100
        expected_orders = 2
        orders = Orders()
        orders.max_stops = 2
        how_many = orders.combine_orders(requests, n_max)
        self.assertEqual(how_many, expected_orders)

    def test_minor_n_max(self):
        requests = [120, 10, 30, 10]
        n_max = 20
        expected_orders = 9
        orders = Orders()
        orders.max_stops = 2
        how_many = orders.combine_orders(requests, n_max)
        self.assertEqual(how_many, expected_orders)

    def test_larger_values(self):
        requests = [1200000]
        n_max = 50000
        expected_orders = 24
        orders = Orders()
        orders.max_stops = 1
        how_many = orders.combine_orders(requests, n_max)
        self.assertEqual(how_many, expected_orders)

    def test_empty_requests(self):
        requests = []
        n_max = 20
        orders = Orders()
        with self.assertRaises(InvalidRequest) as exc:
            orders.combine_orders(requests, n_max)
        self.assertEqual(exc.expected.message, "Invalid values in requests list. Must be a List of positive integers.")

    @parameterized.expand([([10, "req"],), ([10, -1],), ([10, 12.45],), ([10, 0],), ([10, False],)])
    def test_any_data_type_for_requests(self, requests):
        """Tests the validation of different type of data (str, negative int, float, zero, bool)
           for requests lists.
        """
        n_max = 2
        orders = Orders()
        with self.assertRaises(InvalidRequest) as exc:
            orders.combine_orders(requests, n_max)
        self.assertEqual(exc.expected.message, "Invalid values in requests list. Must be a List of positive integers.")

    @parameterized.expand([("n_max",), (-1,), (12.45,), (0,), (False,)])
    def test_any_data_type_for_n_max(self, n_max):
        """Tests the validation of different type of data (str, negative int, float, zero, bool)
           for n_max value.
        """
        requests = [70, 30, 10]
        orders = Orders()
        with self.assertRaises(InvalidNMax) as exc:
            orders.combine_orders(requests, n_max)
        self.assertEqual(
            exc.expected.message, "Invalid values for maximum stops quantity. Must be a positive integer."
        )


if __name__ == "__main__":
    unittest.main()
