from cerberus import Validator

from question2 import constants
from question2.exceptions import InvalidRequest, InvalidNMax


def validate_requests(requests):
    """Validates data from requests"""

    v = Validator(constants.REQUESTS_VALIDATOR)

    if not v.validated({"requests": requests}):
        raise InvalidRequest


def validate_n_max(n_max):
    """Validates data for n_max"""

    v = Validator(constants.N_MAX_VALIDATOR)

    if not v.validated({"n_max": n_max}):
        raise InvalidNMax
