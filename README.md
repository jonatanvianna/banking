
[![CircleCI](https://circleci.com/gh/circleci/circleci-docs.svg?style=svg)](https://circleci.com/bb/jonatanvianna/banking/)

#### Sicredi Code Challenge
Code implementation for `question1.py` and `question2.py`.

It runs all unit tests using `pytest` for both questions implementation.

##### To get and run the project:

```bash
$ git clone https://bitbucket.com/jonatanvianna/banking.git
$ cd banking
$ docker build . --build-arg ENVIRONMENT=prod --tag banking
$ docker run banking
```